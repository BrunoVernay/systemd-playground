#include <iostream>
#include <unistd.h>

#include <systemd/sd-daemon.h>

int main() {
	int sleep_time = 2;

    for (int i = 0 ; i < 100 ; i++) {
        std::cout << " Notifying every " << sleep_time << "s" << std::endl;
        sd_notify(0, "WATCHDOG=1\n");
		sleep(sleep_time);
        sleep_time += 1;
    }
    std::cout << "We should never reach this point !!" << sleep_time << "s.";
    exit(0);
}

