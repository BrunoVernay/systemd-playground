# Watchdog and Notification

Demonstrate the use of Watchdog and notification.

The service is configured with a watch dog of a few seconds. A C++ program notify the watchdog "I am alive" with increased delay.

References:
- Watchdogs (2012) http://0pointer.de/blog/projects/watchdog.html
- https://www.freedesktop.org/software/systemd/man/systemd.service.html#WatchdogSec=
- Simple test code from systemd https://github.com/systemd/systemd/blob/master/src/test/test-watchdog.c
- Simple sample code from [Yocto Intel-Edison](http://git.yoctoproject.org/cgit/cgit.cgi/meta-intel-edison/plain/meta-intel-edison-distro/recipes-support/watchdog-sample/)

## Run

I made multiple folder, with increasing complexity:
- `t1` is simple demo. Illustrate that you can do simple things the simplest way!
- `t2` the program gets the timeout info from the service and use Notification messages
- `t3` illustrates a dependent service waiting for the notification to start. systemd waits for the first notification from the program, and only then starts the dependents services.

Try to `systemd-analyze service-watchdogs` to get/set the watchdog actions. Very useful to debug!
```
...
juin 28 11:10:47 sample[8507]:  Notifying every 7s
juin 28 11:10:54 systemd[1]: sample.service: Watchdog disabled! Ignoring watchdog timeout (limit 6s)!
juin 28 11:10:54 sample[8507]:  Notifying every 8s
juin 28 11:11:01 systemd[1]: sample.service: Watchdog disabled! Ignoring watchdog timeout (limit 6s)!
juin 28 11:11:02 sample[8507]:  Notifying every 9s
...

```


## systemd notify and Watchdog API

The systemd's watchdog leverages the wider [notify API](https://www.freedesktop.org/software/systemd/man/sd_notify.html#). The `sd_notify()` functions allows the program to inform systemd about its status.

systemd provides an API that allow the program to know the timeout duration defined in the service definition. `sd_watchdog_enabled()` https://www.freedesktop.org/software/systemd/man/sd_watchdog_enabled.html

It is possible to send more information to systemd. It can then display a status and messages. The service must be configured to allow access via `NotifyAccess` https://www.freedesktop.org/software/systemd/man/systemd.service.html#NotifyAccess=  It is used in the program *t2*.

Service type `notify` means that systemd will *wait* until the program send a notification to consider that it has started and that it can start *dependent* services. But the Watchdog does NOT require the `notify` type.

The restart on *t2* is configured to `Restart=on-watchdog` specifically. But there are [7 restarts options](https://www.freedesktop.org/software/systemd/man/systemd.service.html#Restart=).

## Information

`wdctl` gives information about the *hardware* Watchdog. (Might not work in a VM)
```
$ sudo wdctl
Device:        /dev/watchdog
Identity:      iTCO_wdt [version 0]
Timeout:       30 seconds
Pre-timeout:    0 seconds
Timeleft:      30 seconds
FLAG           DESCRIPTION               STATUS BOOT-STATUS
KEEPALIVEPING  Keep alive ping reply          1           0
MAGICCLOSE     Supports magic close char      0           0
SETTIMEOUT     Set timeout (in seconds)       0           0
```

`systemd-analyze service-watchdogs` to get/set the watchdog actions. Very useful to debug!

## Watchdog vs. Timeouts

There are many timeouts durations that can be configured in the service definition: Start time, Stop time, reload ... and different behavior for each cases.

They are used in [02-restart](02-restart/Readme.md). 


## Dependencies

Dependencies and service life-cycle can be tricky.

In *t3* the goal is to have dep-A dependent on sample.

- the `run.sh` only starts *dep-A*. (By design)
- The sample service has no mention of dep-A. (Loose coupling.) (note that information will be added dynamically about the dependency.)
- `Requires`, `After` and `PartOf` are required for:
  - *sample* to start when *dep-A* starts
  - *dep-A* to stop or restart when *sample* stop or restart
  - Note that if *sample* starts it will NOT start *dep-A*
- `WantedBy` in `[Install]` would be required for *dep-A* to start when *sample* start.
- With `Requisite` *dep-A* won't even starts. It does not wait for the * sample* to get up. It fails immediately.
- With `BindTo` the services start, but *dep-A* does not recover once *sample* fails.
- `Restart=no` is not a problem on the *dep-A* service. When *sample* goes down, systemd stops *dep-A* and when *sample* is restarted (because `Restart=on-watchdog` for it), then systemd starts *dep-A*.


## Timeout extent (buy me more time)
TODO With the Notification API: extend the timeout https://www.freedesktop.org/software/systemd/man/sd_notify.html#EXTEND_TIMEOUT_USEC=%E2%80%A6

> Specially useful, when the process takes more time than usual: disk verification, DB reconstructing an index ...

## Background info

The hardware watchdog device `/dev/watchdog` is single-user only. Either enable this in systemd, or use a separate external watchdog daemon: https://linux.die.net/man/8/watchdog


## System Watchdog

`RuntimeWatchdogSec=` option in `/etc/systemd/system.conf`. It defaults to 0 (no watchdog). Set it to 20s to turn on supervision by the hardware of systemd and the kernel beneath it. After 20s of no keep-alive pings, the hardware will reset itself. 
Note that systemd will send a ping to the hardware at half the specified interval, i.e. every 10s.

`ShutdownWatchdogSec=` can be configured in `/etc/systemd/system.conf`. It controls the watchdog interval to use during reboots.
It defaults to 10min, and adds reliability to the reboot logic: if a clean reboot is not possible and shutdown hangs, the watchdog hardware resets the system abruptly, as extra safety net.

Ref: https://www.freedesktop.org/software/systemd/man/systemd-system.conf.html#RuntimeWatchdogSec=

### Embedded use case

Extract from a recent thread in systemd's mailing list:
https://lists.freedesktop.org/archives/systemd-devel/2017-October/039652.html
https://github.com/systemd/systemd/issues/7063


