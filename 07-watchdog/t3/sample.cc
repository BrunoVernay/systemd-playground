#include <iostream>
#include <unistd.h>

#include <systemd/sd-daemon.h>

int main() {
	int sleep_time_s;                    // Sleep time interval in s
    uint64_t watchdog_interval_micro_s;  // Watchdog from service definition in µs !!
    int ret;
    
    std::cout << "Hello, this is sample program speaking to the logs !"<< std::endl;

    ret = sd_watchdog_enabled(0, &watchdog_interval_micro_s);
    if (ret<0) {
        std::cerr << "Could not get Watchdog Interval from systemd!!"<< std::endl;
        sd_notify(0, "STATUS=Failed to start up\n");
        exit(1);
    }
    if (ret==0) {
        std::cerr << "Looks like no watchdog are defined in the service ?!"<< std::endl;
        sd_notify(0, "STOPPING=1\nSTATUS=No point to start up\n");
        sleep(5);
        exit(0);
    }

    sleep_time_s = watchdog_interval_micro_s/ (1000*1000);
    std::cout << "The service watchdog is set to "<< sleep_time_s << "s"<< std::endl;
    sleep_time_s /= 2;     // Recommandation is to set the value to half the interval

    std::cout << "I am waiting for 10 sec now, count !"<< std::endl;
    sleep(10);
    std::cout << "Bored? Let's go then."<< std::endl;

    sd_notify(0, "READY=1\n"
                  "STATUS=Processing nothing, but anyway ...\n");

    for (int i = 0 ; i < 100 ; i++) {
        std::cout << " Notifying every " << sleep_time_s << "s" << std::endl;
        sd_notify(0, "WATCHDOG=1\n");
		sleep(sleep_time_s);
        sleep_time_s += 1;
    }
    std::cout << "We should never reach this point !!" << sleep_time_s << "s.";
    exit(0);
}

