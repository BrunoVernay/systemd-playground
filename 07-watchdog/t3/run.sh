#!/bin/bash -x
# This will install a simple systemd service and activate it

sudo systemctl stop sample.service
sudo systemctl stop dep-A.service

g++ sample.cc -o sample $(pkg-config --cflags --libs libsystemd)
chmod +x sample

sudo cp sample /usr/local/bin/
sudo cp sample.service /etc/systemd/system/
sudo cp dep-A.service /etc/systemd/system/

sudo systemctl daemon-reload
sudo systemctl start dep-A.service

systemctl status sample -l --no-pager
systemctl status dep-A -l --no-pager
journalctl --no-hostname -fu sample.service -u dep-A.service

