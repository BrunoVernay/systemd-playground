# Keyring

Demo the systemd helpers to use the kernel keyring.

Show how to pass credentials to a  service with the kernel keyring instead of environement variables or files. (See https://lists.freedesktop.org/archives/systemd-devel/2018-November/041671.html )

Example: `KeyringMode` https://www.freedesktop.org/software/systemd/man/systemd.exec.html#KeyringMode=

On the command line: http://man7.org/linux/man-pages/man7/session-keyring.7.html 


## Usage

Typical usage will be:
- Kerberos caching tickets https://k5wiki.kerberos.org/wiki/Projects/Keyring_collection_cache
- NOT SSH, because it has its own set of tools already (ssh-agent, [Keychain](https://www.funtoo.org/Keychain), ssh-ident, [pam_ssh](https://linux.die.net/man/8/pam_ssh), ...).
- Replace environment variable or command line parameters to communicate secrets.
  - https://diogomonica.com/2017/03/27/why-you-shouldnt-use-env-variables-for-secret-data/
  - http://movingfast.io/articles/environment-variables-considered-harmful/
  - https://blog.fortrabbit.com/how-to-keep-a-secret
  - Why you may have to communicate secrets? Asking for them can be a separate task: https://www.ict.griffith.edu.au/anthony/info/crypto/passwd_input.txt
- Handling interactive system level secrets. Disk encryption passphrase for example
  - https://www.freedesktop.org/software/systemd/man/systemd-ask-password.html
  -  https://www.freedesktop.org/wiki/Software/systemd/PasswordAgents/

## Tutorial

Start playing with:
```
keyctl show @us
cat /proc/keys
cat /proc/key-users
```

- Start with the [Kernel Keyring Tutorial](Keyrings.md). It does not relate to systemd, but demonstrate keyring.
- In [t1](t1) A service gets its secret from the persistent keyring

### Set keyx for a service

The service only care about a specific keyring. This keyring must have been filled by whatever means before hand (reading from a file or asked to the user, or else ...)

It is even in the https://github.com/systemd/systemd/blob/master/TODO :
> add a concept for automatically loading per-unit secrets off disk and inserting them into the kernel keyring. Maybe SecretsDirectory= similar to ConfigurationDirectory

*TBD* ...


## Sources

Most of the information in these pages comes from dicussions in https://lists.freedesktop.org/archives/systemd-devel/

