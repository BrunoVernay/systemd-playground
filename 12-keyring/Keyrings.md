# Notes about Kernel Keyrings

Beware, this is a work in progress!!!

## In-kernel key management and retention facility

_The Linux key-management facility is primarily a way for various kernel components to retain or cache security data, authentication keys, encryption keys, and other data in the kernel._

- It is NOT a solution to store data that should survive a reboot.
- Its primary usage is for kernel components

But, _interfaces are provided so that user-space programs can ... use the facility for their own purposes_ 

Keyring has Acccess Control that goes further than usual:
```
               | propagation control | life-cycle control | access control
---------------+---------------------+--------------------+----------------
kernel keyring | YES YES YES YES YES | YES YES YES YES YE | YES YES YES YE
files          | NO NO NO NO NO NO N | NO NO NO NO NO NO  | YES YES YES YE
environment    | NO NO NO NO NO NO N | NO NO NO NO NO NO  | NO NO NO NO NO
```


You will save some time reading about sessions, possessions and a few other concepts.

## Cheat Sheet

About the rights "alswrv" for key / keyring (https://github.com/Distrotech/keyutils/blob/9d52b8ab86931fb5a66fa5b567ea01875f31016e/keyctl.c#L328) : 
- a 0x20 Set attribute:  change permissions and ownership
- l 0x10 Link make links to a key
- s 0x08 Search
- w 0x04 Write the payload / add or remove links to keys
- r 0x02 Read the payload / list content
- v 0x01 View the attributes

They appear in 4 groups: 
1. Possessor
2. User
3. Group
4. Others (any)

`keyctl describe <key>` and `cat /proc/keys` will show the 4 groups.

## Hands-on

You should read a bit about Kernel Keyring *before* starting this or it will be a bit difficult to follow!

### Session keyring
 
The "Session" is the easiest to work with from the bash.

See the related script: `t01-keyring.sh`: create, read and revoke a key.

You can learn the basic commands, but that does not help much.

For exemple, start another login shell and try to read the key:
`sudo --login -u $USER keyctl print <Key-ID>` ("Permission denied").

That is because a new shell means a new Session keyring:
`sudo --login -u $USER keyctl show @s`

Notice that each time you launch the previous command, there is a new identifier for the session's keyring.

### User's keyring

Let's try another keyring (See the related script: `t02-keyring.sh`):

```
# Create a key named "testkey" of type "user" containing "testdata" in the @user's keyring
keyctl add user testkey testdata @u 

# You should see your key in the list:
keyctl list @u

# And the whole hierarchy
keyctl show @us

# Get your key identifier
export MyKeyID=$(keyctl search @u user testkey) 
echo $MyKeyID
```

Would like to read it?
```
keyctl read $MyKeyID

# Cannot read your own key? Describe it at least:
keyctl describe $MyKeyID 

# also see it here, with permission in hex. (3f means all permissions)
cat /proc/keys 
```
Why:
- the user keyring is searched (As per the [documentation: Searching for Key](http://man7.org/linux/man-pages/man7/keyrings.7.html) the search goes up to the `user-session-keyring` in the hierarchy.) It is not a search issue.
- We created the key, we possess it, right? WRONG!
- We do not *possess* the key!
- and only the possessor has the Read right, not the User ...

### Possession

Continuing on the previous experience, related script: `t03-keyring.sh`.

It feels counter-intuitive, because we created the key. But read about [possession](http://man7.org/linux/man-pages/man7/keyrings.7.html) "_a thread possesses its Session, Process and Threat keyrings_". That's all! No `@u`ser keyring and no `@us` User-Session keyring either!!

Using `keyctl describe` we see that as a `user` we have total control `0x3f` on `@u` and `@us`. This is why we could *create* the key: User have Write right on the `@u`ser keyring. 
But on our created key the permission are `alswrv-----v...`. As a user we can only view (its attributes). 
Even `keyctl setperm @u 0x3f3f3f3f ; keyctl describe @u` will not improve the situation!

A solution is to link the `@u`ser's keyring under the `@s`ession keyring. It will give us *possession* of the key! (Because, again, a thread possesses its Session, Process and Thread keyrings.)
```
keyctl show @s
keyctl link @u @s
# Now we possess the key and since the Possessor has Read permission ...
keyctl print $MyKeyID
keyctl show @s
```

We can read the key. And to really prove the point:

```
# As possessor, we have the right to set attributes, We can give the User read permission
keyctl describe $MyKeyID 
keyctl setperm  $MyKeyID 0x3f030000 
keyctl describe $MyKeyID 

# Unlink the key, we are not Possessor anymore
keyctl unlink @u @s
keyctl show @s 
# As a user, we can read the key
keyctl print $MyKeyID
```

Note you can, as a possessor, remove your own read right: `keyctl setperm $MyKeyID 0x3d000000`. 


*TODO*: ??? "_The user session keyring is searched by request_key(2) if the actual session keyring does not exist and is ignored otherwise_" http://man7.org/linux/man-pages/man7/user-session-keyring.7.html

*Note* If the behavior looks strange, it is because we are testing via the shell, but it is not the intended way to use the keyring. Wait for better use-cases ...


Now log out and in again.  All your keys have disappear.

See `man user-keyring` and then `man persistent-keyring`

### Persistent keyring

"Persistent" does not mean accross reboot, but across user's sessions.

```
export keyringID=$(keyctl newring kr-t-1 @s)
keyctl add user k_in_sess mySecret $keyringID
keyctl show @s
keyctl get_persistent $keyringID
keyctl show @s

```

See [the test case "t1"](t1): it leverages the Persistent keyring to pass secret to a service.

Note that the persistent keyring gives setAttr rights to no one. 
 
## Use-cases

Note that use-cases related to systemd are in the [Readme.md](Readme.md). 

### Set credentials that last securely until reboot

Inspired from https://mjg59.dreamwidth.org/37333.html

As a user with sudo, I want to set a key, give it to root (other users with sudo) and no one else.

```
export KIRO=$(sudo keyctl add user testkey testdata @s)
keyctl describe $KIRO
```

We created the key with user UID=0 (root), but in the current session:
```
keyctl show @s
sudo keyctl show @s
``` 

As a user, we have setAttr right and we are possessor of the key, since it is in the session keyring.
*TODO*: Explain why it does not work without sudo despite being Possessor ...
```
sudo keyctl setperm $KIRO 0x3f3f0000

```
We can link the key to the root's user keyring and remove it from the session.
```
sudo keyctl link   $KIRO @u
sudo keyctl unlink $KIRO @s
```

*TODO* Demonstrates usefullness 
- for users (without sudo) the key is out of reach, they cannot even know it exists. (TODO: `keyctl describe $KIRO` gives Permission denied, showing it exists ...)
- for user (with sudo) the key is available whatever login they use, since it is in the root's `@u`ser keyring.

```
keyctl print $KIRO
sudo keyctl print $KIRO
```



## References

- Ref
  - Man page http://man7.org/linux/man-pages/man7/keyrings.7.html
  - Kernel doc https://www.kernel.org/doc/Documentation/security/keys.txt ([Same but HTML](https://01.org/linuxgraphics/gfx-docs/drm/security/keys/))
  - The kernel security subsystem manual (contains 30 pages on the subject) https://mchehab.fedorapeople.org/kernel_docs_pdf/security.pdf
- a bit more didactic:
  - Working with the kernel keyring https://mjg59.dreamwidth.org/37333.html
  - Caching Passwords for a Time http://www.ict.griffith.edu.au/anthony/info/crypto/passwd_caching.txt
  - Kerberos caching tickets https://k5wiki.kerberos.org/wiki/Projects/Keyring_collection_cache
  - Kerberos Credential Thievery http://delaat.net/rp/2016-2017/p97/report.pdf 
- About Environment Variable usage
  - https://diogomonica.com/2017/03/27/why-you-shouldnt-use-env-variables-for-secret-data/
  - http://movingfast.io/articles/environment-variables-considered-harmful/
  - https://blog.fortrabbit.com/how-to-keep-a-secret
- Actual code
  - Kerberos https://github.com/krb5/krb5/blob/master/src/lib/krb5/ccache/cc_keyring.c 
  - pam gdm https://gitlab.gnome.org/GNOME/gdm/commit/31ed6f2b3f1ab45ae07aad41c13a51ba91fd159d#d1fb07b09061ff7dad8e2c05a8cec15e64a8814c
  - DogTagPKI https://github.com/dogtagpki/nuxwdog/blob/master/src/com/redhat/nuxwdog/wdpwd.cpp

Searching the Web, I found very few code examples: Kerberos, FreeIPA, OpenAFS ... this is not very used yet.

> Searching the Web, there have been some [back](https://github.com/systemd/systemd/issues/5522) [and](https://github.com/systemd/systemd/pull/6275) [forth](https://github.com/systemd/systemd/pull/6286) on the subject in 2017. Quickly, it has to do with services (sshd, httpd, ...) who would have mixed-up all the user's keyrings.

## See also

Other projects related to Keys, Secrets and credentials.
- *libsecret* is a library for storing and retrieving passwords and other secrets. It communicates with the "Secret Service" using D-Bus. gnome-keyring and ksecretservice are both implementations of a Secret Service.
    - https://developer.gnome.org/libsecret/unstable/ Simple password storage and lookup
    - https://gitlab.gnome.org/GNOME/libsecret  "GObject based library for accessing the Secret Service API"
- *GNOME Keyring* is a collection of components in GNOME that store secrets, passwords, keys, certificates and make them available to applications  https://wiki.gnome.org/Projects/GnomeKeyring
- *GNOME Seahorse* GUI application for managing encryption keys and passwords in the GNOME Keyring.  https://wiki.gnome.org/Apps/Seahorse
- SSH and GPG tools: ssh-agent, [Keychain](https://www.funtoo.org/Keychain), ssh-ident, [pam_ssh](https://linux.die.net/man/8/pam_ssh), ...

You should also have a look at Yubico, TPM, Secure Element ...

