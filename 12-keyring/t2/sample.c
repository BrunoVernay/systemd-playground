/**
 * Simple C program that get a secret from the kernel keyring
 * - C++ has issue when linking ???
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <keyutils.h>


int main() {

    /** 
     * TODO Check if get_persistent is required (or searching is enough)
     * TODO Pretty print permissions! http://man7.org/linux/man-pages/man3/keyctl_describe.3.html
     * TODO See if find_key_by_type_and_name() is easier to work with ?
     * TODO In a "real" program
     *   - the secret would be used and securely removed
     *   - The whole operation would happen in a specific thread (KEY_SPEC_THREAD_KEYRING)
     * TODO See how to make threads in C ...
     */

    const char *type = "user";
    const char *description = "mysecret";
    const char *descrip_lost = "myLostsecret";  // Key set in the run.sh
    void *secret = NULL;
    int secretlen;
    char *info = NULL;

    // Get the keyring for the real UID.
    key_serial_t keyring_id = keyctl_get_keyring_ID( KEY_SPEC_USER_KEYRING, 0);
    if (keyring_id == -1) {
        perror("Get user keyring failed");
        goto cleanup;
    }
    printf( "User Keyring: %lu \n", (long) keyring_id);
    if ( keyctl_describe_alloc(keyring_id, &info) > 0 )  
        printf("Permissions: %s\n", info);  // %s;%d;%d;%08x;%s ??
    free(info); // Always?

    key_serial_t key_id = keyctl_search(keyring_id, type, description, KEY_SPEC_PROCESS_KEYRING);
    if (key_id == -1) {
          perror("Search key failed");
          goto cleanup;
    }
    key_serial_t lostkey_id = keyctl_search(keyring_id, type, descrip_lost, KEY_SPEC_PROCESS_KEYRING);
    if (lostkey_id == -1) {
          printf("Search lost key failed (as expected)\n");
    } else {
          perror("Search lost key SHOULD have failed !!!???");
    }

    // Now the key is in our process keyring.  We can remove it from the user.
    // http://man7.org/linux/man-pages/man3/keyctl_link.3.html
    if ( keyctl_unlink(key_id, keyring_id) == -1 ) {
        perror("Could not unlink the key!");
    }

    printf( "Secret key: %li \n", (long) key_id);
    if ( keyctl_describe_alloc(key_id, &info) > 0 )  
        printf("Permissions: %s\n", info);  // %s;%d;%d;%08x;%s ??
    free(info); // Always?

    secretlen = keyctl_read_alloc(key_id, &secret);
    if (secretlen == -1) {
        perror("Read failed");
        goto cleanup;
    }

    printf("Secret is: %s ", (char*) secret);

cleanup:
    free(secret);
    return 0;
}

