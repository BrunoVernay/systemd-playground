
## Use case

In this example a systemd managed service:
- requires a secret. Imagine it has to access some database or else
- It does not want to manage the secret.
  - It expects the secret to be readily available in its user's keyring
  - being in the keyring does NoT allow to restart (crash or timer) without loosing the secret
- how the secret is put in the keyring is not the service's business
  - Some may want to ask the user
  - Some may get it from a file on the system
  - Another service might get it from a Vault https://www.VaultProject.io/    

The service's process removes the key from the user keyring. (See the C code). The kernel will not allow anyone (not even root) to get the secret. The program should remove the secret from its memory as soon as possible to let the Kernel keep it safe (Look at the speific precautions taken by the kernel: it does not put in swap for example ...)


## Status

Currently:
- the service runs with a custom *system user* "samser"
- and a *shared Keyring*

I set the secret in the service's user keyring from the service definition file as `preStart`.

It works.


## Todo


### C++
I could not build a C++ program with `keyutils`. Looks like it does not have the required `pkg-config` stuff???

