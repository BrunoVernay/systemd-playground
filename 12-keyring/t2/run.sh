#!/bin/bash
# This will build and install a simple systemd service and activate it

set -x
## Clean-up
sudo systemctl stop sample.service


## Installation, Setup
# Might want to install keyutils-libs-devel
#g++ sample.cc -o sample "$(pkg-config --cflags --libs keyutils )" || exit $?
cc sample.c -o sample -lkeyutils -Wall  || exit $?
chmod +x sample

sudo useradd --system samser

sudo cp sample /usr/local/bin/
sudo cp sample.service /etc/systemd/system/


## Put the secret key "mysecret" in the service user "samser" keyring 
# This part should be done securely! This is for demonstration only: 
sudo -u samser keyctl add user myLostsecret "I am useless" @u
sudo -u samser keyctl show @u

# This secret is lost as systemctl will start another user session!
# Same as if you logout / login.

## Start the service

sudo systemctl daemon-reload
sudo systemctl start sample.service

systemctl status sample -l --no-pager


sudo -u samser keyctl show @u
k_in_sess_ID=$(sudo -u samser keyctl search @u user mysecret)
#sudo -u samser keyctl print $k_in_sess_ID

