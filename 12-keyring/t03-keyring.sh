#!/bin/sh -v

# Create keys                     <type>   <desc>     <data>  <keyring>
export k_in_user_3_ID=$(keyctl add user   k_in_user_3 Scrt-UU-U @u)
keyctl show @s
keyctl describe $k_in_user_3_ID
keyctl print    $k_in_user_3_ID

# Put the key in a keyring that we possess (Session keyring)
keyctl link @u @s
keyctl show @s
# since the Possessor has Read permission ...
keyctl print $k_in_user_3_ID

# Possessor can set attributes
keyctl setperm  $k_in_user_3_ID 0x3d000000
keyctl describe $k_in_user_3_ID
# We removed read permission to the possessor
keyctl print $k_in_user_3_ID

# Give User  Read access
keyctl setperm  $k_in_user_3_ID 0x3d030000
keyctl describe $k_in_user_3_ID

# Unlink the key, we are not Possessor anymore
keyctl unlink @u @s
keyctl show @s
# As a user, we can read the key
keyctl print $k_in_user_3_ID

# Clean-up 
keyctl  unlink $k_in_user_3_ID @u

