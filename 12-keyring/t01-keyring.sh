#!/bin/sh -v

# Create key  <type> <desc>     <data>   <keyring>"
keyctl  add   user   k_in_sess  mySecret @s

# You should see your key in the list:
keyctl list @s
keyctl show @s

# Note it is not here: 
keyctl show @us

# and here too 
#SERIAL   FLAGS  USAGE EXPY PERM     UID   GID   TYPE      DESCRIPTION: SUMMARY
cat /proc/keys

# Get your key identifier
export k_in_sess_ID=$(keyctl search @s user k_in_sess)

keyctl print $k_in_sess_ID

keyctl describe $k_in_sess_ID


# Clean up
keyctl revoke $k_in_sess_ID
keyctl reap
# keyctl purge user

# Check
keyctl show @s
