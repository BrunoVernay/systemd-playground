#!/bin/sh -v

# Create key <type> <desc>    <data>    <keyring>"
keyctl  add  user   k_in_user Secret-UUU @u

# You should see your key in the list:
keyctl list @u

# The whole hierarchy
keyctl show @us
keyctl show @s

# and here too 
#SERIAL   FLAGS  USAGE EXPY PERM     UID   GID   TYPE      DESCRIPTION: SUMMARY
cat /proc/keys

# Get your key identifier
export k_in_user_ID=$(keyctl search @u user k_in_user)

keyctl print $k_in_user_ID

keyctl describe $k_in_user_ID


# Try to clean up, but without permissions ..
keyctl revoke $k_in_user_ID

# At least, we can
keyctl  unlink $k_in_user_ID @u

# Check
keyctl show @us
keyctl show @s
