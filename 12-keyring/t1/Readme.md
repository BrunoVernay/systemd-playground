
## Use case

In this example a systemd managed service:
- requires a secret. Imagine it has to access some database or else
- It does not want to manage the secret.
  - It expects the secret to be readily available in its user's persistent keyring
  - being in the persistent keyring allows to restart (crash or timer) without loosing the secret
- how the secret is put in the keyring is not the service's business
  - Some may want to ask the user
  - Some may get it from a file on the system
  - Another service might get it from a Vault https://www.VaultProject.io/    


## Status

Currently:
- the service runs with a custom *system user* "samser"
- and a *shared Keyring*

I set the secret in the service's user persistent keyring from my administrative account, via my sudo power.

It works.

## Limitations

The service is just a process running with a specific user. 

If the process can access the persistent keyring and read the secret (It is the goal) then:
There is no way to discriminate that from another process launched via sudo that would impersonate this user.

It means that if I have sudo rights, I will always be able to read the secret later on.


It would have been nice to protect from the administrative account being hacked days or month after having set the secret. But it is not coherent.

I must give up either:
- having sudo right to impersonate the user
- Allow the service to stop and start without re-asking for secret. (Note that the service is free to simply remove the secret from the persistent keyring or configure the timeout.)

There could be workarounds: 
- configure a limited set of sudo commands (only allow to set secret)
- Do not use the persistent keyring and keep the service user keyring alive by running another dummy service that do nothing and never stop ...


## Todo


### C++
I could not build a C++ program with `keyutils`. Looks like it does not have the required `pkg-config` stuff???

## Notes

### Linking persistent keyring

At first I used to put the service user persistent keyring in its own user keyring.
```
sudo -u samser keyctl get_persistent @u
```
and then to link the whole service user keyring in the session.
```
sudo -u samser keyctl link @u @s
```

But it can be done directly: `sudo keyctl get_persistent @s $(id -u samser)` puts "samser" persistent keyring in the @session. 
