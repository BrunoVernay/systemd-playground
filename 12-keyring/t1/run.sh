#!/bin/bash
# This will build and install a simple systemd service and activate it

set -x
## Clean-up
sudo systemctl stop sample.service


## Installation, Setup
# Might want to install keyutils-libs-devel
#g++ sample.cc -o sample "$(pkg-config --cflags --libs keyutils )" || exit $?
cc sample.c -o sample -lkeyutils -Wall  || exit $?
chmod +x sample

sudo useradd --system samser

sudo cp sample /usr/local/bin/
sudo cp sample.service /etc/systemd/system/


## Put the secret key "mysecret" in the service user "samser" persistent keyring 
# This part should be done securely! This is for demonstration only: 
export PKEY_ID=$(sudo keyctl get_persistent @s $(id -u samser))
sudo -u samser keyctl describe $PKEY_ID
sudo -u samser keyctl add user mysecret "I love chocolate" $PKEY_ID
keyctl show @s
sudo -u samser keyctl setperm $PKEY_ID 0x000b0000
sudo -u samser keyctl unlink $PKEY_ID  @s

## Start the service

sudo systemctl daemon-reload
sudo systemctl start sample.service

systemctl status sample -l --no-pager


## Epilogue, imagine someone hacked my (admin) account, could be days after
sudo keyctl get_persistent @s $(id -u samser)
export HACK_KEY_ID=$(sudo -u samser keyctl search @s user mysecret)
# The following wont work since I remove my own right to set Attributes on the key :-)
sudo -u samser keyctl setperm $HACK_KEY_ID  0x3f000000
keyctl show @s
# But read right is still present. No way arround this one.
sudo -u samser keyctl print $HACK_KEY_ID

