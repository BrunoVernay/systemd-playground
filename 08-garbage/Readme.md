# Garbage collection

Illustrate Unit's Resources management

This is about systemd's resources. Typically a Unit will store number of failures, restarts and the current state. 

Ref:
- https://www.freedesktop.org/software/systemd/man/systemd.unit.html#Unit%20Garbage%20Collection
-
-

A service can ask systemd to manage and store, Sockets, but also *file descriptors*: 
- https://www.freedesktop.org/software/systemd/man/sd_pid_notify_with_fds.html#FDSTORE=1
- https://www.freedesktop.org/software/systemd/man/systemd.service.html#FileDescriptorStoreMax=

A use case is to get the file back after a program crash for example.


We could also point out: `StopWhenUnneeded=yes` https://www.freedesktop.org/software/systemd/man/systemd.unit.html#StopWhenUnneeded= "_unit will be automatically cleaned up if no other active unit requires it_".
