
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{

	printf("I am the sample program in C printing this.\n");
	fflush(NULL);

	printf("And now I fork, which should settle my fate ...\n");
	fflush(NULL);
	sleep(1);

	fork();
	printf("You should not see this because I'm dead.\n");

	return 0;
}
