#!/bin/bash -x
# This will install a simple systemd service and activate it
  
sudo systemctl stop sample.service
sudo killall sample
  
  
gcc sample.c -o sample
chmod +x sample
  
sudo cp sample /usr/local/bin/
sudo cp sample.service /etc/systemd/system/
  
sudo systemctl daemon-reload
sudo systemctl start sample.service
  
systemctl status sample -l --no-pager
set +x
echo -e "Run telnet 127.0.0.1 180  It should connect and close.\\n"
journalctl --no-hostname -fu sample.service

