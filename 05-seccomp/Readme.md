# SecComp filters with systemd

Demonstrate the use of SecComp filters ... in progress.

The number of system calls (300+) is way larger than the capabilities (20), but systemd offers _predefined sets_. It is also possible to have white and black list and systemd will merge them. Finally, instead of terminating, SecComp can just raise a signal (Similar to SELinux Permissive mode). 

Reference:
- https://www.freedesktop.org/software/systemd/man/systemd.exec.html#System%20Call%20Filtering 


## Run

The program taken from an existing example simply tries to fork.
Without SecComp filter defined in the service, it shows:
```
mai 09 14:32:40 systemd[1]: Started My sample service.
mai 09 14:32:40 sample[16156]: I am the sample program in C printing this.
mai 09 14:32:40 sample[16156]: And now I fork, which should settle my fate ...
mai 09 14:32:41 sample[16156]: You should not see this because I'm dead.
mai 09 14:32:41 sample[16156]: You should not see this because I'm dead.
```

With SecComp defined:
```
$ journalctl --no-hostname -fu sample.service
mai 09 14:04:42 systemd[1]: Started My sample service.
mai 09 14:04:42 systemd[1]: sample.service: Main process exited, code=killed, status=31/SYS
mai 09 14:04:42 systemd[1]: sample.service: Failed with result 'signal'.
```
```
$ journalctl --no-hostname -f /usr/local/bin/sample
mai 09 14:04:42 audit[14069]: SECCOMP auid=4294967295 uid=0 gid=0 ses=4294967295 subj=system_u:system_r:unconfined_service_t:s0 pid=14069 comm="sample" exe="/usr/local/bin/sample" sig=31 arch=c000003e syscall=12 compat=0 ip=0x7f5470e27e07 code=0x0
mai 09 14:04:42 audit[14069]: ANOM_ABEND auid=4294967295 uid=0 gid=0 ses=4294967295 subj=system_u:system_r:unconfined_service_t:s0 pid=14069 comm="sample" exe="/usr/local/bin/sample" sig=31 res=1
```
Note that the Unit `sample.service` does not display very useful info. You have to query the journal for the program `/usr/local/bin/sample` itself. 

In audit message we see the info: `... syscall=12 ...` and `$ ausyscall 12` gives `brk` which we can add to the filter ... then it crashes on `21` which is `access` ; we add to the filter ...   

At that point I decide to run
```
$ sudo ./create-seccomp-profile -s -c ./sample
SystemCallFilter=newfstat read write access openat close mprotect brk munmap nanosleep exit_group clone mmap arch_prctl
```

Even with all these, I still have a crash on `fstat`, but once it is added, everything worked. *Why has it been missed??!!!* (Running `strace` gave the good result, maybe `perf` or the script itself are flawed?)

Now, guessing that `clone` might be involved in fork, I remove it to check that we execute until the fork.
And it works as expected, displaying `printf()` until the `fork()`.

Changing the `SystemCallErrorNumber` in the service to `EPERM`, `EACCES` or `ENOSYS` allows the program to execute the `fork()`. BUT, 
- There is nothing is the logs that indicates that `clone` was called. Maybe it would be possible with an `auditd` rule?
- It does not work on all syscall! If I only filter `brk`, the program terminate with `error while loading shared libraries: libc.so.6: cannot open shared object file: Operation not permitted`!

## SecComp filters

SecComp is a wide subject. Here we focus only on its relation/interaction with systemd.

SecComp is available on *ARM* with kernel v3.10.

There are 2 versions of SecComp! The first one introduced in 2005 has been extended in 2012 with a seccomp2 or "mode 2" or `seccomp-bpf` with _similar functionality, more flexibility and higher performance_. Systemd used the later almost as soon as it was created.

- [Kernel doc](https://www.kernel.org/doc/Documentation/prctl/seccomp_filter.txt): "_System call filtering isn't a sandbox_". 
- Other kernel doc: http://www.infradead.org/~mchehab/kernel_docs/userspace-api/seccomp_filter.html
- [Mozilla](https://wiki.mozilla.org/Security/Sandbox/Seccomp): "_It's a simple sandboxing tool_" 
- https://docs.docker.com/engine/security/seccomp/ 

BPF filters can also be sequences of system calls! Seems like Chromium might leverage this advanced feature.

## What syscall are you using?

What SecComp filters does a program require to run? What tools can help to find out? Unlike _Capabilities_, there is no simple answer.

There are multiple ways:
- Run the program with `strace` or `perf` and listing all the calls. `strace -c -f -S name ./sample` (even `strace -o tmp.txt -c -f -S name ./sample > /dev/null ; cat tmp.txt | tail -n +3 | head -n -2 | awk '{print $(NF)}' | tr '\n' ' '`) 
- Use a dedicated script to help: https://prefetch.net/blog/2017/11/27/securing-systemd-services-with-seccomp-profiles/ (the script simply wrap `perf`and outputs the used system calls.)
- Set a filter, but in "warn-only" mode and log all the errors. 

But you still have to make sure to go through all branches of your program. Also, programs usually depends on libraries `glibc` ... that may change over time. 

TODO: As mentioned it is possible to run seccomp in a Permissive "warn-only" mode. But even then, it might be up to the program to log the signals and where they are raised. It seems like it may be possible to use `SECCOMP_RET_TRACE` to notify a kind of `ptrace` about the program's syscall.
See also `SECCOMP_RET_LOG` and `SECCOMP_FILTER_FLAG_LOG` from the man page it is not ultra clear http://man7.org/linux/man-pages/man2/seccomp.2.html . 
It seems that currently, systemd won't help here. One has to run the program maybe with a wrapper to log the calls.


- Interesting how Mozilla handles SecComp https://wiki.mozilla.org/Security/Sandbox/Seccomp They log the error and terminate the process. One can set Mozilla in Permissive "warn-only" mode on the command line. 

## Getting information from the command line

To  know if your system support SecComp: `grep SECCOMP /boot/config-$(uname -r)`

Is a process filtered? `grep Seccomp /proc/<pid>/status`

0. means `SECCOMP_MODE_DISABLED`
1. means `SEC‐COMP_MODE_STRICT` 
2. means `SECCOMP_MODE_FILTER`

You can list all: `grep -i seccomp /proc/*/status | grep -v 0$`

There are these 2 files
```
$ cat  /proc/sys/kernel/seccomp/actions_avail
kill_process kill_thread trap errno trace log allow
$ cat  /proc/sys/kernel/seccomp/actions_logged
kill_process kill_thread trap errno trace log
```

What is this system call number? `ausyscall <num>` convert names/numbers and also knows about architectures x86 ...

What system calls are in a set? `systemd-analyze syscall-filter @raw-io` outputs all or just the specified set. 

