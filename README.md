# systemd-playground

Playground and information about systemd.
You can quickly experiment with your own modifications.

These are intented to be run on a Linux system with systemd already installed.
Type `systemctl poweroff` (or `systemctl is-system-running`) if unsure.

Demo are in specific folders:
- [01-dependency](01-dependency/Readme.md) about dependencies (using simple scripts)
- [02-restart](02-restart/Readme.md) about restart (using C++ program)
- [03-capability](03-capability/Readme.md) about capabilities and users (using C to listen to socket) 
- [04-socket](04-socket/Readme.md) about Socket management by systemd
- [05-seccomp](05-seccomp/Readme.md) about SecComp filter.
- [06-quota](06-quota/Readme.md) about quota, cgroups 
- [07-watchdog](07-watchdog/Readme.md) about Watchdogs and Notifications 
- [08-garbage](08-garbage/Readme.md) about garbage collection ... in progress 
- [09-dbus](09-dbus/Readme.md) about D-Bus ... in progress 
- [10-types](10-types/Readme.md) about Types (simple, forking, idle, dbus ...) ... in progress 
- [11-file_access](11-file_access/Readme.md) about file access, Dynamic User, Environment.
- [12-keyring](12-keyring/Readme.md) about kernel keyrings management, keyctl ... in progress

There is a `cleanup.sh` to remove the files installed uring the demo

Remember: [systemd index](https://www.freedesktop.org/software/systemd/man/systemd.directives.html#Colophon) contains 2563 entries in 13 sections, referring to 253 manual pages!


## Start, Stop and Reload signals

`systemctl ...`
- *Start* simply start the program specified in the [Unit].
- *Stop* send a kill signal: SIGTERM and 90s later SIGKILL ([Can be configured](https://www.freedesktop.org/software/systemd/man/systemd.kill.html#Options)).
- *reload* execute [ExecReload=](https://www.freedesktop.org/software/systemd/man/systemd.service.html#ExecReload=) if it has been defined. See below. 
- *restart* stop and start, but the subtlety is that resources (files) are not flushed out.
- *try-reload-or-restart* ... as always, many many options in systemd


Ref: 
- https://www.freedesktop.org/software/systemd/man/systemd.kill.html

Specific commands can be launched at start, stop and reload: See [01-dependency](01-dependency/). [ExecStartPre=, ExecStartPost=](https://www.freedesktop.org/software/systemd/man/systemd.service.html#ExecStartPre=), [ExecStop=](https://www.freedesktop.org/software/systemd/man/systemd.service.html#ExecStop=) and [ExecStopPost=](https://www.freedesktop.org/software/systemd/man/systemd.service.html#ExecStopPost=). 

To see what is usually done: `systemctl show \*.service --property=ExecReload |grep -v ^$`
```
systemctl show \*.service --property=ExecReload |grep -v ^$ |sort| uniq     
ExecReload={ path=/bin/kill ; argv[]=/bin/kill -HUP $MAINPID ; ignore_errors=no ; start_time=[n/a] ; stop_time=[n/a] ; pid=0 ; code=(null) ; status=0/0 }
ExecReload={ path=/bin/kill ; argv[]=/bin/kill -s HUP $MAINPID ; ignore_errors=no ; start_time=[n/a] ; stop_time=[n/a] ; pid=0 ; code=(null) ; status=0/0 }
ExecReload={ path=/bin/kill ; argv[]=/bin/kill -SIGHUP $MAINPID ; ignore_errors=no ; start_time=[n/a] ; stop_time=[n/a] ; pid=0 ; code=(null) ; status=0/0 }
ExecReload={ path=/bin/kill ; argv[]=/bin/kill -USR1 $MAINPID ; ignore_errors=no ; start_time=[n/a] ; stop_time=[n/a] ; pid=0 ; code=(null) ; status=0/0 }
ExecReload={ path=/sbin/iscsiadm ; argv[]=/sbin/iscsiadm -m node --loginall=automatic ; ignore_errors=yes ; start_time=[n/a] ; stop_time=[n/a] ; pid=0 ; code=(null) ; status=0/0 }
ExecReload={ path=/sbin/multipathd ; argv[]=/sbin/multipathd reconfigure ; ignore_errors=no ; start_time=[n/a] ; stop_time=[n/a] ; pid=0 ; code=(null) ; status=0/0 }
ExecReload={ path=/usr/bin/dbus-send ; argv[]=/usr/bin/dbus-send --print-reply --system --type=method_call --dest=org.freedesktop.DBus / org.freedesktop.DBus.ReloadConfig ; ignore_errors=no ; start_time=[n/a] ; stop_time=[n/a] ; pid=0 ; code=(null) ; status=0/0 }
ExecReload={ path=/usr/bin/dbus-send ; argv[]=/usr/bin/dbus-send --print-reply --system --type=method_call --dest=org.freedesktop.NetworkManager /org/freedesktop/NetworkManager org.freedesktop.NetworkManager.Reload uint32:0 ; ignore_errors=no ; start_time=[n/a] ; stop_time=[n/a] ; pid=0 ; code=(null) ; status=0/0 }
ExecReload={ path=/usr/sbin/avahi-daemon ; argv[]=/usr/sbin/avahi-daemon -r ; ignore_errors=no ; start_time=[n/a] ; stop_time=[n/a] ; pid=0 ; code=(null) ; status=0/0 }
ExecReload={ path=/usr/sbin/exportfs ; argv[]=/usr/sbin/exportfs -r ; ignore_errors=no ; start_time=[n/a] ; stop_time=[n/a] ; pid=0 ; code=(null) ; status=0/0 }
```

## Security info

A general Security analysis has been added in v240 (dec. 2018)
`systemd-analyze security` for analyzing the
          security and sand-boxing settings of services in order to determine an
          "exposure level" for them, indicating whether a service would benefit
          from more sand-boxing options turned on for them.

There is a good post on this topic https://www.ctrl.blog/entry/systemd-service-hardening.html (Jan 2020)

Since there is a special focus on Security, it is worth to know about PolicyKit https://www.freedesktop.org/software/systemd/man/systemd-ask-password.html  AKA "polkit — Authorization Manager".

## GUI info

Specific GUI are indicated in each of the topics pages.

Generally, commands start with `systemd-...`  There is *auto-completion* on all commands!

GUI: on *Ubuntu* there is `systemadm` and on Fedora and others there is [Cockpit](https://cockpit-project.org/) which also works on remote machines.

## Time Synchro

Security has a huge dependency on time synchronization.  There is not a lot to play with, but you should be aware of it.

Just a few starting points: systemd provides `systemd-timesyncd.service` a trivial SNTP client. But generally distributions rely on `chronyd.service` instead. Just issue a `timedatectl -a` command to get information.

Which synchro service are you running? What NTP servers are configured? Is priority given to the server obtained via DHCP? Is there an "enterprise" proxy blocking access to NTP servers? Will you be alerted if your clock start drifting too much? ...

## Others playgrounds

There are others publicly available playgrounds:
- Rust https://github.com/mizzy/rust-systemd-playground 
- "A demo playground for systemd-exec named descriptor (fd:)" https://github.com/lucab/systemd-named-fd 
- Java https://github.com/thjomnx/java-systemd-playground
- ...


