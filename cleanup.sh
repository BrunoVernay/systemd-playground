#!/bin/bash
# Clean up all the installed junk.

sudo systemctl stop sample dep-A dep-B


sudo rm -f /etc/systemd/system/sample.*
sudo rm -f /etc/systemd/system/sample@.service
sudo rm -f /etc/systemd/system/dep-A.service
sudo rm -f /etc/systemd/system/dep-B.service
sudo rm -fr /etc/systemd/system.control/sample.service.d/ 
sudo rm -f /usr/local/bin/sample

sudo systemctl unset-environment SAMPLE_TARGET_DIR
sudo rm -rf /var/local/sample-*
sudo rm -rf /var/lib/private/sample-*
sudo rm -rf /var/lib/systemd/sample-*

sudo userdel -rZ samser 

sudo systemctl daemon-reload

journalctl --no-hostname -n20 -u sample -u dep-A -u dep-B
