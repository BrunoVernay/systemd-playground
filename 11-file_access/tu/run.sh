#!/bin/bash 
# This will install a simple systemd service and activate it


systemctl --user stop sample.service

set -x

g++ sample.cc -o sample
chmod +x sample

mkdir -p ~/.config/systemd/user
sudo cp sample /usr/local/bin/
cp sample.service ~/.config/systemd/user/

systemctl --user daemon-reload
systemctl --user start sample.service

systemctl --user status sample -l --no-pager

sleep 2

journalctl --user --no-hostname -n6 -u sample.service
echo " "

