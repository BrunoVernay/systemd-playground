#!/bin/bash 
# This will install a simple systemd service and activate it

export TARGET_DIR=/var/local/sample-service

sudo systemctl stop sample.service

set -x

g++ sample.cc -o sample
chmod +x sample

sudo cp sample /usr/local/bin/
sudo cp sample.service /etc/systemd/system/

sudo mkdir -p $TARGET_DIR
#sudo touch $TARGET_DIR/test.txt
sudo useradd --system samser
sudo chown -R samser:samser $TARGET_DIR

sudo systemctl daemon-reload
sudo systemctl set-environment SAMPLE_TARGET_DIR=${TARGET_DIR}
sudo systemctl start sample.service

systemctl status sample -l --no-pager

sleep 2

journalctl --no-hostname -n6 -u sample.service
echo " "
cat $TARGET_DIR/*.txt

