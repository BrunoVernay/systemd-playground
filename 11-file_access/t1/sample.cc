#include <iostream>
#include <unistd.h>
#include <fstream>



int main(int argc, char *argv[], char * envp[]) {
	int sleep_time = 2;

    const char* file_path;

    std::string file_name = "test.txt";
    std::string line;
    std::ofstream myfile;

    if ( !( file_path = std::getenv("SAMPLE_TARGET_DIR") ) )  {
        std::cerr << "Could not get the environment variable";
        return 1;
    } 

    std::cout << "Open the file " << file_path + ('/'+ file_name)  << std::endl;
    myfile.open (file_path + ('/'+ file_name) );

    if (myfile.is_open()) {
        std::cout << "Write the file\n";
        myfile << "ABC 123\n";
        for (int i=0; envp[i] != NULL; i++)
            myfile << envp[i] << std::endl;
        std::cout << "Close the file\n";
        myfile.close();
    }
    else {
        std::cerr << "Could not open the file!";
        return 1;
    }

	sleep(sleep_time);
    return 0;
}

