#!/bin/bash 
# This will install a simple systemd service and activate it


sudo systemctl stop sample.service

set -x

g++ sample.cc -o sample
chmod +x sample

sudo cp sample /usr/local/bin/
sudo cp sample.service /etc/systemd/system/

sudo systemctl daemon-reload
sudo systemctl start sample.service

systemctl status sample -l --no-pager

sleep 2

journalctl --no-hostname -n6 -u sample.service
echo " "

