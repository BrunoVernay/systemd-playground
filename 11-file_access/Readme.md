# File access

Demo a service writing and reading files. It passes parameters with Environment variables. 

- File access is restricted to specific directories (Read/Write)
- In `t1` we experiment with Dynamic User. 
- In `tu` we experiment with user space systemd. 
- TODO: How to let systemd manage the file descriptor
- TODO: Tests temporary files
- TODO: test `UMask` directive

## Run

> Do not forget to `../cleanup.sh`, because this one is a bit more invasive (creates user, files ...)

It writes a file in a directory specified in `run.sh`.

The `t1` folder contains the test with *Dynamic users*.

### ReadWritePaths, 

Theses sets white lists and blacklist paths.
> *Caution*: just using `ReadWritePaths` will not restrict access to other path!
It must be used with `ProtectSystem=strict`


`ReadWritePaths=/... ` What if the path does not exist?
```
Failed to set up mount namespacing: No such file or directory
Failed at step NAMESPACE spawning /usr/local/bin/sample: No such file or directory
Main process exited, code=exited, status=226/NAMESPACE
```
The service will *fail* to start.
Put a `-` before the path, it tells systemd to ignore non-existent path.


### ProtectSystem

`ProtectSystem=` offers a cumulative levels:
- *No* (Default) : no restrictions
- *Yes* : `/usr`, `/boot` ro
- *full* : `/etc` ro
- *strict* : *All* ro except `/dev` `/proc` `/sys` 

In all cases it protects against _write_ access only.  You can always read.

It is usually used in combination with `ReadWritePaths` (See above)

## Dynamic user

systemd has a very secure concept of Dynamic user. It came with a lot of very secure default parameters, but as their `ID` will change file access is an issue.

TODO: systemd has solution:
`RuntimeDirectory=, StateDirectory=, CacheDirectory=, LogsDirectory=, ConfigurationDirectory` https://www.freedesktop.org/software/systemd/man/systemd.exec.html#RuntimeDirectory=

BUG: There is a bug in Fedora 28 that breaks file access with Dynamic user: https://bugzilla.redhat.com/show_bug.cgi?id=1559286 The solution is known, but not successfully deployed yet. 
Still, that is 2 bugs in Fedora 28 impairing "DynamicUser" functionality. (See 03-Capabilities.)

## C++ & files

Note: Trying to write to a file that does not yet exist with `fstream` does not work. Either you `touch` it beforehand or you use `ofstream`.

## Passing parameters with environment variables

To avoid hard-coding the PATH in the program. I use environment variables:

1. Set in the `run.sh`
2. Transmitted to systemd globally `systemctl set-environment ...`
3. Transmitted to the program in the .service definition `PassEnvironment=...`
4. Used in the program `std::getenv(...)`
5. Globally unset in the `../cleanup.sh` `systemctl unset-environment ...`


