# Dependencies 

Very simple test to start using systemd. (More info about dependencies in *07-Watchdog / t3*)

The program is a simple online script embedded in the service definition file. The programs display their PID, so it is easy to kill them.

There is a more advanced test in `t1` folder. to demonstrate the `oneshot` Type.

Sample depends on
- dep-A depends on
   - dep-B

Simply provide:
- creating a service depending on 2 others services
- Starting it
- The "program" is a trivial script that you can modify

You can then play with them:
- Kill the program  and systemd will restart it automatically and its dependencies
- You can kill only the dependency and systemd will restart them.

You can easily change the script or the dependencies.

## Run

Launch `./run.sh`. It will install and start the services.  It will also display instructions to see logs and kill PID.

`journalctl -f --no-hostname -u sample -u dep-A -u dep-B` Allows to see the logs of all services.
You can then open another terminal and issue `kill <pid>` or `systemctl stop sample` to see the dependencies at work.

## Info

`systemctl list-dependencies` will show dependencies, but only one level (it is not recursive).

Test also:
- `systemctl list-dependencies dep-A`
- `systemctl list-dependencies --reverse dep-A`

There are multiple kind of dependencies: `systemctl show -p Requires,Wants,Requisite,BindsTo,PartOf,Before,After dep-A`

For example
```
sudo systemd-analyze critical-chain sample.service

sample.service +5ms
└─basic.target @5.521s
  └─sockets.target @5.521s
    └─sssd-kcm.socket @5.520s
      └─sysinit.target @5.510s
        └─systemd-update-utmp.service @5.502s +7ms
          └─auditd.service @5.457s +43ms
            └─systemd-tmpfiles-setup.service @5.413s +41ms
              └─fedora-import-state.service @5.386s +26ms
                └─local-fs.target @5.383s
                  └─var-lib-docker-overlay2.mount @9.512s
                    └─local-fs-pre.target @5.282s
                      └─lvm2-monitor.service @1.892s +3.389s
                        └─lvm2-lvmetad.service @5.254s
                          └─lvm2-lvmetad.socket @1.891s
                            └─-.mount
                              └─system.slice
                                └─-.slice
```

It is even possible to display a graph: `systemd-analyze dot sample.service dep-A.service dep-B.service | dot -Tsvg > circular.svg`

![normal.svg](./normal.svg)

## Requires, Requisite, Wants, BindsTo, PartOf, Before, After, Conflicts ...

See: https://www.freedesktop.org/software/systemd/man/systemd.unit.html#Requires= 

Find a good graph to explain the variations @TODO.

`Requires`is kind of soft, as it will not be stopped by a circular dependency for example.
`Requisite`is more strict. It will not start services if there is a circular dependencies.

There are all kind of variations with `Wants`, `BindsTo` ... Here is a short summary: 
- *Requires* If this service gets activated, the units listed here are activated too. If one of the dependent services fails to activate, systemd does not start this service. 
- *Wants* Similar to Requires, except failed units do not have any effect on the service.
- *BindsTo* Similar to Requires, except stopping the dependent units also stops the service.
- *PartOf* Similar to Requires, except the stopping and restarting dependent units also stop and restart the service.
- *Conflicts* A list of unit names that, if running, cause the service not to run.
- *Before, After* A list of unit names that configures the ordering of dependencies between services.
- *OnFailure* A list of unit names that are activated when this service enters a failed state.
- *PropagatesReloadTo, ReloadPropagatedFrom* A list of unit names that will receive reload request too. Otherwise reload request are not propagated.

## Conditions to skip dependencies

systemd provides a set of conditions that can be used to silently skip a Unit, without marking it as failed. 
It might be useful to dynamically remove service dependency on specific architecture, in case of virtualisation, if a certain kernel parameter is provided ...
See: https://www.freedesktop.org/software/systemd/man/systemd.unit.html#ConditionArchitecture=

TODO: Test !!!

## Type=oneshot

This type of service is demonstrated in the `t1` folder.

systemd waits for the program to finish before starting the dependent services.

- If `Type=simple` the sample starts immediately after its dependence *started*. We can see the sample outputs before the dependency "dep-A" finishes.
- If `Type=oneshot` the sample starts only after the dependency has *finished*. We can see the sample outputs only after the dependency "dep-A" finishes.

Trick: in case of restart, the dependency is started again too. One can use `RemainAfterExit=yes` and systemd will consider dep-A active *even* when all its processes finishes ... In this case, sample will restart without starting and waiting dep-A.

## Circular dependencies detection

We can create a circular dependency:
1. open dep-B.service 
2. uncomment the dependency on sample. 

If we use `Requires` It still runs, nothing particular is happening. Because it is soft (look at the docs). If you replace `Requires` by `Requisite` in all 3 services, then it does not run anymore.

Messages are clear in the log, but the verification tools are not very helpful.
You should see this in the logs:
```
systemd[1]: Found ordering cycle on ....
...
Dependency failed for Very simple test service.
sample.service: Job sample.service/start failed with result 'dependency'.
...
Breaking ordering cycle by deleting job  ...
```

The documentation points to using: `systemd-analyze verify sample.service` ! (In both cases it did not show any output. Maybe it only check the file syntax, and not all dependencies.)

In the "soft"case, the graph shows the issue: `systemd-analyze dot sample.service dep-A.service dep-B.service | dot -Tsvg > circular.svg` (If you know which services to query and see the loop not even in red.)

![circular.svg](./circular.svg)

In the "hard" case to graph is empty. It must be installed and running to produce the graph.

References:
- RFE https://github.com/systemd/systemd/issues/3829 
- https://unix.stackexchange.com/questions/193714/generic-methodology-to-debug-ordering-cycles-in-systemd

