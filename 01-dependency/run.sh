#!/bin/bash -x
# This will install a simple systemd service and activate it

sudo cp ./*.service /etc/systemd/system/

sudo systemctl stop sample.service
sudo systemctl stop dep-A.service
sudo systemctl stop dep-B.service
sudo systemctl daemon-reload
sudo systemctl start sample.service

systemctl status sample -l --no-pager
sleep 2
systemctl status dep-A -l --no-pager
systemctl status dep-B -l --no-pager

set +x
echo -e "\\nRun this: journalctl -f --no-hostname -u sample -u dep-A -u dep-B"
echo -e "In another terminall kill the given pid. (it should restart)"

