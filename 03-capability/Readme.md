# Capabilities - Open a socket

The C program listen on port 180 and exit as soon as someone connects.

Since `restart=on-success` it restarts automatically.

2 points:
- The capability is set in the service file. Comment it and the service does not run
- Since the service is configured to restart immediately, it is important to configure the socket so it can be reused. That is the point of the `setsockopt()` in the C program. 

There are 2 ways to define the user running the service:
- Specify `User`and `Group` (Ex. `User=nobody`)
- Leverage `DynamicUser=true` (it works if SELinux is not enforced. Might be a bug in current Fedora 27)

## CapabilityBoundingSet vs. AmbientCapabilities
 
CapabilityBoundingSet are useful to *remove* capabilities. While running as root (simply not using `User` nor `DynamicUser`), we keep only `net_bind_service` capabilities instead of `full`.
```
pscap
ppid  pid   name        command           capabilities
1     11142 root        sample            net_bind_service
```

AmbientCapabilities are useful to *add* capabilities. While running as an unprivileged user (using `User` or `DynamicUser`), we get:
```
pscap
ppid  pid   name        command           capabilities
1     12531 sample      sample            net_bind_service +
```

## Run

Below are 2 methods to run the program. 
Either way, once it runs, connect via `telnet 127.0.0.1 180` .

The program terminates as soon as the connection is done

### Automatic

Just run `./run.sh`

### You can build and run the program manually 

Build 
```
gcc sample.c -o sample
chmod +x sample
```

Run `sudo ./sample`

## What capabilities are we using?
 
To know what capabilities a program is using:
- [Linux bcc Tracing Security Capabilities (2016)](http://www.brendangregg.com/blog/2016-10-01/linux-bcc-security-capabilities.html)
- or Install [libcap-ng](https://people.redhat.com/sgrubb/libcap-ng/) and use `pscap`.

I will use  BCC project and its `capable` program (Install `bcc` and `bcc-tools` packages).

1/ Launch capable :
```
sudo /usr/share/bcc/tools/capable |grep sample
11:13:26  0      6104   sample           10   CAP_NET_BIND_SERVICE 1
```
2/ Launch the sample program as root. Note: the capabilities does not appear immediately, just a few seconds after. 

Warning: It only sees the Capabilities exercised. The program contains a `chroot()`, but since we do not go into the if, the required capability `CAP_SYS_CHROOT` is not seen!

Note: `pscap` is not a solution for this use-case, because we run `sudo ./sample` as would any candid developer. So it has `full` capabilities being root.
```
> pscap
1     1693  bruno2      gnome-keyring-d   ipc_lock +
1     2465  root        fwupd             chown, dac_override, dac_read_search, fowner, fsetid, kill, setgid, setuid, setpcap, linux_immutable, net_bind_service, net_broadcast, net_admin, net_raw, ipc_lock, ipc_owner, sys_rawio, sys_chroot, sys_ptrace, sys_pacct, sys_admin, sys_boot, sys_nice, sys_resource, sys_time, sys_tty_config, mknod, lease, audit_write, audit_control, setfcap, mac_override, mac_admin, syslog, wake_alarm, block_suspend, audit_read
850   5049  root        dhclient          dac_override, kill, setgid, setuid, net_bind_service, net_admin, net_raw, sys_module, sys_chroot, audit_write
5203  6524  root        sudo              full
6524  6525  root        sample            full

```

## About Socket reuse

Not relevant concerning capabilities, but for info ...

You can try to comment the `setsockopt()` function is the C program. Then it works on first launch, but as soon as we connect, it restarts and it says `bind: Address already in use`!
The issue being the socket left in a `TIME_WAIT` state that can last long. 

Ref:
- https://stackoverflow.com/questions/5106674/error-address-already-in-use-while-binding-socket-with-address-but-the-port-num
- https://stackoverflow.com/questions/10619952/how-to-completely-destroy-a-socket-connection-in-c
- http://www.softlab.ntua.gr/facilities/documentation/unix/unix-socket-faq/unix-socket-faq-2.html#time_wait

## About capabilities

Obviously, it is better to add capabilities to a non-privileged user than to remove them to root! 

- Counter argument about capabilities https://forums.grsecurity.net/viewtopic.php?f=7&t=2522&sid=c6fbcf62fd5d3472562540a7e608ce4e#p10271
