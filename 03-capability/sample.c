#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdio.h>
#include <unistd.h>

int main(void) {
    int server_fd;
    struct sockaddr_in server_addr;
    int option = 1;

    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    server_addr.sin_port = htons(180);

    setbuf(stdout, NULL);   // Print immediately, no buffer

    // Testing if the detection capability program can see this:
    // if (chroot("/") != 0)  printf("Cannot chroot!");

    printf(" Socket ... ");
    if ((server_fd = socket(PF_INET, SOCK_STREAM, 0)) == -1) {
        perror("socket");

        // Testing if the detection capability program can see this:
        if (chroot("/") != 0)  printf("Cannot chroot!");

        return 1;
    }
    // Allows to reuse the socket immediately
    setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));

    printf(" Binding ... ");
    if (bind(server_fd, (struct sockaddr*) &server_addr, sizeof(server_addr)) == -1) {
        perror("bind");
        return 1;
    }

    printf(" Listening ... ");
    if (listen(server_fd, 10) == -1) {
        perror("listen");
        return 1;
    }

    struct sockaddr_in client_addr;
    socklen_t client_addrlen = sizeof(client_addr);

    printf(" Accepting \n");
    int client_fd = accept(server_fd, (struct sockaddr*) &client_addr, &client_addrlen);
    if (client_fd == -1) {
        perror("accept");
        return 1;
    }
    puts("Connection accepted");

    printf(" Closing ");
    shutdown(client_fd, 0);
    close(client_fd);
    shutdown(server_fd, 0);
    close(server_fd);
    printf(" Closed\n");
    return 0;
}

