# Running a C program

The C program counts down to 0 and exit. Since `restart=on-success` it restarts automatically.


In the *07-watchdog / t2* there is a `Restart=on-watchdog` to play with.

Reference:
- There is a nice table here: https://www.freedesktop.org/software/systemd/man/systemd.service.html#Restart=
-

## Run

Launch `./run.sh` (Will install and run the service.)
Use `../cleanup.sh`  to remove it.

You should see the logs with the counter decrementing and the program restarting regularly.

In another terminal, you can also kill the program (its PID is shown in the logs)
- It restart if `kill _is_pid_`
- It does NOT restart if `kill -9 _is_pid_`

`systemctl stop sample` is the polite way to stop the service.


You can uncomment the `StartLimitIntervalSec` to see that the service will stop restarting after a while.
You should see:
```
systemd[1]: sample.service: Start request repeated too quickly. 
systemd[1]: sample.service: Failed with result 'start-limit-hit'.
systemd[1]: Failed to start My sample service.
```
 


## Exit status and signals

It is possible to configure a list of specific exit status that will trigger either a restart or prevent a restart. `SuccessExitStatus`, `RestartPreventExitStatus`, `RestartForceExitStatus` [doc](https://www.freedesktop.org/software/systemd/man/systemd.service.html#SuccessExitStatus=).

## Restart limits, Burst

We can configure how many time `StartLimitBurst` the service is permitted to restart in a window period `StartLimitIntervalSec`. Then define an action: `reboot`, `poweroff-immediate` ... or just no more restarts.

## Delays, Timeouts

One can configure many TimeOuts and Delays: 
- `RestartSec` sleep time before restarting the service. [doc](https://www.freedesktop.org/software/systemd/man/systemd.service.html#RestartSec=)
- `TimeoutStartSec` if the service did not start, it is considered failed and shut down. [doc](https://www.freedesktop.org/software/systemd/man/systemd.service.html#TimeoutStartSec=)
- `TimeoutStopSec` same idea, the program will get `SIGTERM` and then `SIGKILL`. [doc](https://www.freedesktop.org/software/systemd/man/systemd.service.html#TimeoutStopSec=)
- `RuntimeMaxSec` Max time to run, before being terminated. [doc](https://www.freedesktop.org/software/systemd/man/systemd.service.html#RuntimeMaxSec=)


## ExecStopPost and lifecycle

Notice that ExecStopPost is executed when the process is restarted by systemd, even if the *service* has not been explicitly `systemdctl` restarted.


