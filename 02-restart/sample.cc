// Decrement slowly and exit with return code 0.
 
#include <iostream>
#include <unistd.h>
 
int main()
{
    int MAX_I = 10;
    int i;
    for (i = MAX_I ; i > 0 ; i--)  {
        std::cout << "I will quit in ... " << i << "/" << MAX_I << std::endl;
        sleep(1);
    }
  std::cout << "I quit with return code "<< i << std::endl;
  return i;
}

