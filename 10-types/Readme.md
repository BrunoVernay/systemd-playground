# Types


There are various _type_ of services:
- *simple* - The service starts as the main process. This is the default.
- *forking* - The service calls forked processes and run as part of the main daemon.
- *oneshot* - Similar to simple, except the process must exits before systemd starts follow-up services. Example in [01-dependency](../01-dependency/Readme.md)
- *dbus* - Similar to simple, except systemd wait for the daemon to acquire a name on the D-Bus bus.
- *notify* - Similar to simple, except the daemon sends a modification message using sd_notify or an equivalent call after starting up. Example in [07-watchdog](../07-watchdog/Readme.md) 
- *idle* - Similar to simple, except the execution of the service is delayed until all active jobs are dispatched.

Doc:
- See: https://www.freedesktop.org/software/systemd/man/systemd.service.html#Type=
- Good examples: https://www.freedesktop.org/software/systemd/man/systemd.service.html#Examples


