#include <cstdlib>
#include <iostream>
#include <climits>
#include <limits>
#include <time.h>
#include <unistd.h>

class MyNumPunct : public std::numpunct<char>
{
protected:
    virtual char do_thousands_sep() const { return '.'; }
    virtual std::string do_grouping() const { return "\03"; }
};


int main()
{
    unsigned long p;

    //std::cout.imbue(std::locale( std::locale::classic(), new MyNumPunct ));

    try {
         for (p=ULONG_MAX; p>100 ; p--) {
             unsigned long a = p / time(NULL);
             unsigned long b = (p+a) / time(NULL);
             for (unsigned long i = ULONG_MAX; i>2; i--) {
                 if ((p+b) % i == 0)  std::cout << "Found " << p << " x "  << " mod"  <<  std::endl;
             }
         }   
    }
    catch (...) {
        std::cerr << "Unrecognized exception" << std::endl;

    }

   sleep(1);
   return 0;
}

