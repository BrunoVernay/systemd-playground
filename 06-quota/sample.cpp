#include <cstdlib>
#include <iostream>
#include <limits>
#include <unistd.h>

class MyNumPunct : public std::numpunct<char>
{
protected:
    virtual char do_thousands_sep() const { return '.'; }
    virtual std::string do_grouping() const { return "\03"; }
};

std::size_t  mallocbsearch(std::size_t lower, std::size_t upper)
{
    //std::cout<<"["<<lower<<", "<<upper<<"]\n";
    if(upper-lower<=1)
    {
        std::cout<<"Found! "<<lower<<"\n";
        return lower;
    }
    std::size_t mid=lower+(upper-lower)/2;
    void *ptr=std::malloc(mid);
    if(ptr)
    {
        free(ptr);
        mallocbsearch(mid, upper);
    }
    else
        mallocbsearch(lower, mid);
}

int main()
{
    int *ptr[1024] ;
    int array_size = 1024*1024;
    int size = ( array_size * sizeof(int) ) / 1024;  // in kilo bytes ??
    std::size_t max_mem = 0;

    std::cout.imbue(std::locale( std::locale::classic(), new MyNumPunct ));

    max_mem = mallocbsearch(0, std::numeric_limits<std::size_t>::max());

    try {
         for (int i = 1; i<100 ; i++) {
             std::cout << "Allocating " << i << " x " << size << " \ttotal " 
                 << size*i << " ko"  <<  std::endl;
             ptr[i] = new int[array_size];
             for (long j=0; j<array_size; j++)  ptr[i][j] = 1234;
             sleep(1);
         }   
    }
    catch ( std::bad_alloc &memmoryAllocationException ) {
       std::cerr << "Memory allocation exception occurred: "
           << memmoryAllocationException.what() << std::endl;
   }
    catch (...) {
        std::cerr << "Unrecognized exception" << std::endl;

    }

   sleep(10);
   return 0;
}

