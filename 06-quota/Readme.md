# Quota, CGroups and Accounting

Demonstrate the use of Quota, Slices and CGroups

References:
- Must read: https://www.freedesktop.org/wiki/Software/systemd/ControlGroupInterface/ 
- Resource control unit settings https://www.freedesktop.org/software/systemd/man/systemd.resource-control.html


## Background info
 
### cgroups a bit of history
 
Linux Control Groups kernel concept organizes processes in a hierarchical tree of named groups for the purpose of resource (CPU, Memory, IO) management.

Manage labelled groups of processes for the purpose of monitoring and controlling them and their resource.

The *unified* [cgroup-v2](https://www.kernel.org/doc/Documentation/cgroup-v2.txt) hierarchy is the new (2015) kernel interface. There is a legacy [cgroup-v1](https://www.kernel.org/doc/Documentation/cgroup-v1/cgroups.txt) (2004), that doesn't allow safe delegation of controllers to unprivileged processes. 

Conclusion: beware pre-2015 documentations or blog posts.

systemd does a mapping between its properties in configuration files and kernel's cgroups interfaces.

### Kernel requirements

Since it depends on the settings `cpu.cfs_period_us` and `cpu.cfs_quota_us`, the Kernel needs to be compiled with config-flag `CONFIG_CFS_BANDWIDTH`.

### Slices, Scopes, Services organisation

- Resource limits may be set on `services`, `scopes` and `slices`.
- `slices` regroups `Services` and `scopes`, but cannot contain `processes`.
- `Services` and `scopes` contain processes. They cannot be moved from one `Slices` to another at runtime.

## Run

- main folder, there is a memory quota. Note that RAM accounting does not impact performance and is activated by default.
- In `t1` there is a CPU quota.
- In `t2` there is a Slice with CPU quota


Running by default gives: 
```
 Main PID: 21401 (sample)
    Tasks: 1 (limit: 4915)
   Memory: 300.0K
   CGroup: /system.slice/sample.service
           └─21401 /usr/local/bin/sample
```

With Memory limits:
```
 Main PID: 22772 (sample)
    Tasks: 1 (limit: 4915)
   Memory: 176.0K (high: 170.0K max: 200.0K)
   CGroup: /system.slice/sample.service
           └─22772 /usr/local/bin/sample
```

The program ends up being killed. Todo: see if we can catch any error.
```
mai 15 15:00:09 sample[23000]: Allocating 15
mai 15 15:00:09 systemd[1]: sample.service: Main process exited, code=killed, status=9/KILL
mai 15 15:00:09 systemd[1]: sample.service: Failed with result 'signal'.
```

The amount of memory available, (the number of allocation loops) is not always the same.


Properties can be set at runtime. 
`systemctl set-property httpd.service CPUShares=500 MemoryLimit=500M`
This will create a file in `/etc/systemd/system.control/sample.service.d/...conf` !

Option `runtime` will make the change not persistent:
`systemctl set-property --runtime httpd.service CPUShares=500 MemoryLimit=500M`

### CPU (t1)

CPU test is in `t1` directory.
The sample program will execute some divisions and takes minutes to complete.

The service defines a `CPUQuota=2%`, it should not slowdown you PC.

You can use `top -p <pid>` in a terminal and change the value `sudo systemctl set-property sample.service CPUQuota=80%` !
Remember that `set-property` creates a persistent drop-in file `.conf` by default.

You can also look at `systemd-cgtop system.slice/sample.service`
TODO: Check if it apply to the whole `system.slice`!?

But to see the value, you have to use `systemctl show sample -p CPUQuotaPerSecUSec` ?

> Beware: `ps` and `top` do not use the same %CPU definition. Look at the documentation to understand the difference.

### Slice (t2)

I simply create a `Slice` with the `CPUQuota`.
In the service, I remove the `CPUQuota` and specify the Slice.

Run it and use the previous info to see the difference.

## Info

`systemd-cgls` or `systemd-cgls -u [sample]`.

`systemd-cgtop`
 
## Accounting

Accounting allows to see statistics on CPU, Memory, IO blocks, IP traffic ...

It is shown in the *status* and in the logs. But we can also ask for specific values:
```
systemctl set-property httpd.service IPAccounting=yes
systemctl show httpd -p IPIngressBytes -p IPIngressPackets -p IPEgressBytes
```


See: [IP Accounting and Access List](http://0pointer.net/blog/ip-accounting-and-access-lists-with-systemd.html)

Accounting is generally implied each time we use quota.

RAM accounting does not impact performance and is enabled by default as of v239.


## Disambiguation
 
There is a `systemd-quotacheck` responsible for file system quota checker logic. It runs at boot and only stays if at least one file system has quotas enabled. See: https://www.freedesktop.org/software/systemd/man/systemd-quotacheck.service.html


