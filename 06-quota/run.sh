#!/bin/bash -x
# This will install a simple systemd service and activate it
  
sudo systemctl stop sample.service
sudo killall sample
  
  
g++ sample.cpp -o sample
chmod +x sample
  
sudo cp sample /usr/local/bin/
sudo cp sample.service /etc/systemd/system/
  
sudo systemctl daemon-reload
sudo systemctl start sample.service
  
systemctl status sample -l --no-pager
set +x
journalctl --no-hostname -fu sample.service

