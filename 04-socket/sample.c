#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <systemd/sd-daemon.h>

int main(void) {
    const int BUFFER_SIZE = 2000;
    int client_fd, read_size;
    char client_message[BUFFER_SIZE];

    setbuf(stdout, NULL);   // Print immediately, no buffer

    printf(" Socket ... ");
    if (sd_listen_fds(0) != 1) {
        perror("No or too many file descriptors received.");
        return 1;
    }
    client_fd = SD_LISTEN_FDS_START + 0;
    printf(" #%i.\n", client_fd);
    
    if (sd_is_socket(client_fd, AF_INET6, SOCK_STREAM, 0)) {
        printf("We have an INET v6 TCP socket.\n");
    } else {
        printf("Not sure what kind of socket we have ?\n");
    }
    
    //Receive a message from client
    while( (read_size = recv(client_fd , client_message , BUFFER_SIZE , 0)) > 2 )
    {
        //Send the message back to client
        printf("Receiving: %i bytes.\n", read_size);
        write(client_fd , client_message , read_size);
    }
    puts("Received only 2 bytes. Bye.\n");

    //shutdown(client_fd, SHUT_RDWR);     // Sends TCP FIN
    //close(client_fd);                   // Sends TCP FIN
    // Just leaving the program will sends a TCP FIN too.
    
    return 0;
}

