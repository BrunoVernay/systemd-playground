#!/bin/bash -x
# This will install a simple systemd service and activate it

sudo systemctl stop sample\*
sudo systemctl stop sample.socket
sudo killall sample


gcc sample.c -o sample $(pkg-config --cflags --libs libsystemd)
chmod +x sample

sudo cp sample /usr/local/bin/
sudo cp sample.socket /etc/systemd/system/
sudo cp sample@.service /etc/systemd/system/

sudo systemctl daemon-reload
sudo systemctl start sample.socket

systemctl status sample.socket -l --no-pager

set +x
echo -e "Run telnet 127.0.0.1 180  It should echo until you enter too few characters.\n"
echo -e "run journalctl --no-hostname -fu sample\*"
