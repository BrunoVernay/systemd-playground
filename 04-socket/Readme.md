# Let systemd manage sockets

*systemd* listen on port 180 and launch the C program for each connection. The C program gets its socket from systemd  and (it is a demo) copy input to the output. It exits when the input size is empty (just hit Enter in telnet.).

Reminder: it is good for security, but also for dependencies without bothering about ordering, no messages are lost.

TODO: There are at least 3 others ways to do it:
- redirect `StandardInput` and `StandardOutput` (even simpler!)
- Uses `Accept=false` to let the program manage connections (does not create one instance per connection.)

## Basics

- systemd will match the socket and the service based on their file name. (Unless a `Service=` is specified with another name).
- If `Accept=true` systemd will create one instance of service for each connection. In this case the service name must be `foo@.service`. (Instances will be named `foo@i001.service`, `foo@i002.service`, ...)
- Use `journalctl -u sample\*` to get the logs.

```
$ systemctl list-sockets
LISTEN                                    UNIT                            ACTIVATES
/run/avahi-daemon/socket                  avahi-daemon.socket             avahi-daemon.service
...
[::]:180                                  sample.socket                   sample@5-127.0.0.1:180-127.0.0.1:51270.service, sample@4-127.0.0.1:180-127.0.0.1:51238.service
```
 
## Development

To keep it simple, the C program rely entirely on systemd to get the socket. (Some programs detect if they are running without systemd and open their socket independently, but it requires more code.) 
The include `<systemd/sd-daemon.h>` requires the package `systemd-devel` (I used version 234.)
The program MUST be linked with the `libsystemd`.

## Run
 
1. Simply launch `./run.sh` will build, install and start the *socket only*.
2. A `ps aux | grep sample` will show that no instance of the sample are running, until ...
3. We launch `telnet 127.0.0.1 180`. Then `ps` and the `journalctl` will show that one instance is started. We can type messages in `telnet`. (An empty message will tell the C program to exit.)
4. We can launch and close multiple `telnet` connections, there will be as many instances as there are connections.

To get information:
- `systemctl list-sockets [--all]`   (similar to `systemctl list-timers`)
- `journalctl --no-hostname -f /usr/local/bin/sample` appears to be reliable.
- `journalctl --no-hostname -fu sample\*` does not follow the output like expected!

## Sockets and Netcat details

`ncat` stays open even when the server close and shutdown the connection, which is kind of annoying. You have to Ctrl-C to exit. `telnet` does close as soon as the server close. 

I made some capture to be sure about the behavior:
- When the C program just `close()` the connection, it sends a `FIN` and netcat ACKnowledge it. The netcat still allow to send data, in which case it receives a `RST` (reset). Only if one still tries to send data, then netcat closes with "Broken pipe"!
- When the C program just `shutdown(fd, SHUT_RDWR)`, same behavior.
- When the C program just quit, same behavior. (a TCP FIN is sent somehow).
- Telnet is different. It will respond `FIN` when it receives `FIN`, terminating the connection and itself.
 
Conclusion: Just use telnet for this example.

## References

- http://0pointer.de/blog/projects/socket-activation.html (2011)
  - http://0pointer.de/blog/projects/socket-activation2.html
- http://0pointer.de/blog/projects/socket-activated-containers.html
- Man page (tons of options) https://www.freedesktop.org/software/systemd/man/systemd.socket.html
- systemd-activate to test socket activation https://www.linux.org/docs/man8/systemd-activate.html
-- https://www.usenix.org/system/files/login/articles/login_june_06_jedrzejewski-szmek.pdf (7 pages, PDF, 2015)

## ZeroMQ 

In 2016, ZeroMQ did get a patch to support "external" socket: `use_fd`. 
> When creating a new ZMQ socket, if this option is set the value
> will be used as the File Descriptor instead of allocating a new
> one via the socket () system call.
> int use_fd;
https://github.com/zeromq/libzmq/blob/6bfa91f13f73b67be47ded18986b76d154d5923b/src/options.hpp#L247

This issue gives links and test code [Using existing socket #2530](https://github.com/zeromq/libzmq/issues/2530)

