# D-Bus

TODO: On hold. ZeroMQ is THE messaging BUS. 

systemd has a dependency on D-Bus which is an independent project. systemd uses it a lot and has its own client implementation: `sd-bus` (there are C++ bindings, by independents projects [sdbus-cpp](https://github.com/Kistler-Group/sdbus-cpp)).

systemd uses D-Bus, but D-Bus can also leverage systemd (See [Bus activable services](#bus-activatable-services))

I will focus on sd-bus usage ... A sample C program in a Service `Type=dbus` expose a method. A user space java program call this method.

Reference:
- *Must read*: D-Bus (short read, 2018) https://www.freedesktop.org/wiki/Software/dbus/
- Nice Schemas: https://en.wikipedia.org/wiki/D-Bus    
- *Must read*: comprehensive explanations (2015) http://0pointer.net/blog/the-new-sd-bus-api-of-systemd.html
- sd-bus — A lightweight D-Bus IPC client library https://www.freedesktop.org/software/systemd/man/sd-bus.html
- systemd API doc (2014, must be stable ...) https://www.freedesktop.org/wiki/Software/systemd/dbus/


## Run



## Info

There are graphical tools:
- `bustle` [records and draws](https://www.freedesktop.org/wiki/Software/Bustle/) sequence diagrams of D-Bus activity, showing signals ...
- `d-feet` is a [funny GUI tool](https://wiki.gnome.org/Apps/DFeet) to Debug or simply inspect D-Bus interfaces.

`busctl` gives info about the current bus. It is possible to introspect objects, call methods ... Running `busctl --user` shows a lots of user-spaces applications using D-Bus!

`busctl` can dump the bus's traffic in a `.pcap` file to later analyse it with `Wireshark` !!

`dbus-monitor` will also print information and can dump selected messages in a `pcap` file. You can see a lot of messages in text form while interacting with Gnome Desktop.


## systemd usage of D-Bus

How systemd uses D-Bus:
- systemd can be controlled via D-Bus: starting/stopping services ...
- `Type=dbus` services are considered successfully started once they acquired the `BusName=...` name on the D-Bus (System or Session).
- A program (with or without systemd) can obviously use D-Bus and the `sd-bus` library to send or receive messages.
-

### bus-activatable services

Bus Activable service is *D-Bus concept*. One application can send a message on the Bus and if the recipient service does not exist on the Bus yet,  D-Bus is able to launch it.  D-Bus as a service definition file format, very similar to systemd.

Note: Once a service is instantiated, it is accessible, manageable via D-Bus, because systemd provides the access. Even if *the programs itself does not know anything about D-Bus*.

But if the service is not yet `[Install]`ed by systemd, D-Bus is useless, unless ...
There is a _D-Bus Service_. See [Example 5](https://www.freedesktop.org/software/systemd/man/systemd.service.html#Examples)

- Fedora has [a nice explanation](https://fedoraproject.org/wiki/Packaging:Systemd#Activation)
- [D-Bus message starting services](https://dbus.freedesktop.org/doc/dbus-specification.html#message-bus-starting-services)

